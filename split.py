print("• Bienvenido!!!\nHoy vamos a aprender la Función Split en las cadenas de texto Str ")

cadena = "La felicidad se puede encontar hasta en los momentos más oscuros, solo debemos encender la luz"

print("• La cadena con la que trabajaremos es :", cadena)

print("• La Función Split ayuda a romper una cadena de texto segun un caracter en el que desee separar, este no se incluira cuando se separe; de la siguiente manera: ")

print("• Romper la cadena donde este la letra (a): ", cadena.split("a"))