print("Bienvenido!!!\nHoy vamos a aprender la Función Startswith en las cadenas de texto Str ")

cadena = "python"

print("La cadena con la que trabajaremos es :", cadena)

print("La Función Startswith ayuda a identificar si en una cadena de texto se inicia con unos valores especificos, de la siguiente manera: ")

print("¿La cadena inica con py?", cadena.startswith("py"))
print("¿La cadena inica con lo?", cadena.startswith("lo"))