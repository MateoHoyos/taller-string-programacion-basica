print("• Bienvenido!!!\nHoy vamos a aprender la Función Find en las cadenas de texto Str ")

cadena = "Un carro de color rojo se chocó"

print("• La cadena con la que trabajaremos es :", cadena)

print("• La Función Find ayuda a encontrar la posicion donde incia la parte de la cadena que se quiere y devuelve el primer indicedel elemento \n si no se encuentra devuelve un -1, de la siguiente manera: ")

print("• Encuentre la Posicion de rojo: ", cadena.find("rojo"))
print("• Encuentre la Posicion de camion: ", cadena.find("camion"))