print("Bienvenido!!!\nHoy vamos a aprender la Función Endswith en las cadenas de texto Str ")

cadena = "helado"

print("La cadena con la que trabajaremos es :", cadena)

print("La Función Endswith ayuda a identificar si en una cadena de texto termina con unos valores especificos, de la siguiente manera: ")

print("¿La cadena termina con do?", cadena.endswith("do"))
print("¿La cadena termina con he?", cadena.endswith("he"))