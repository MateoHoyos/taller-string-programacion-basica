print("• Bienvenido!!!\nHoy vamos a aprender el proceso de Slice en las cadenas de texto Str ")

cadena = "Más alla del mar habra un lugar, donde el sol cada mañana brille más"

print("• La cadena con la que trabajaremos es :", cadena)

print("• El proceso de Slice consiste en formas de sacar un conjunto de caracteres a partir de una cadena de texto, mediante las posiciones de la cadena \n estas pueden ser tomadas tambien como negativas, de la siguiente manera: ")

print("• Sacar de la cadena desde la posicion 4 a la 23")
print("• ",cadena[4:23])

print("• Sacar de la cadena desde cualquier posicion a la -5")
print("• ",cadena[:-5])
