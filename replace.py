print("• Bienvenido!!!\nHoy vamos a aprender la Función Replace en las cadenas de texto Str ")

cadena = "Los ojos son la ventana del alma"

print("• La cadena con la que trabajaremos es :", cadena)

print("• La Función Replace ayuda a reemplazar una parte de la cadena de texto por otra, de la siguiente manera:  ")

print("• Reemplazar ojos por OJOS: ", cadena.replace("ojos","OJOS"))
